import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Hardcoded daily power shifts
power_data = [
    ("20.04.15", 0.5, -0.5),
    ("24.04.15", -0.5, 0.5),
    ("25.04.15", -1.5, 1.5),
    ("26.04.15", -1.0, 1.0),
    ("28.04.15", -0.5, 0.5),
    ("30.04.15", 0.3, -0.3),
    ("01.05.15", -1.2, 1.2),
    ("03.05.15", 0.5, -0.5),
    ("04.05.15", -0.3, 0.3),
    ("05.05.15", -0.7, 0.7),
    ("06.05.15", 0.2, -0.2)
]

# Convert to DataFrame
df = pd.DataFrame(power_data, columns=["Date", "Sergei_Power", "Elizaveta_Power"])

# Convert dates to datetime format
df["Date"] = pd.to_datetime(df["Date"], format="%d.%m.%y")

# Compute cumulative power shifts
df["Sergei_Cumulative"] = df["Sergei_Power"].cumsum()
df["Elizaveta_Cumulative"] = df["Elizaveta_Power"].cumsum()

# Display dataframe

# Plotting
plt.figure(figsize=(10, 5))
plt.plot(df["Date"], df["Sergei_Cumulative"], label="Sergei Power", linewidth=2)
plt.plot(df["Date"], df["Elizaveta_Cumulative"], label="Elizaveta Power", linestyle="dashed", linewidth=2)
plt.axhline(0, color="black", linewidth=1, linestyle="dotted")
plt.xlabel("Date")
plt.ylabel("Cumulative Power Shift")
plt.title("Cumulative Power Dynamics Between Sergei and Elizaveta")
plt.legend()
plt.grid(True)

# Show plot
plt.show()

