# Now we will analyze the full dataset from April 20, 2015, to June 6, 2015.
from matplotlib import pyplot as plt
import pandas as pd
# This includes:
# - Extracting all message dates
# - Evaluating power shifts per day with real-number scoring
# - Accumulating the power imbalance over time
# - Plotting the cumulative power imbalance

from datetime import datetime
import numpy as np

# Extract all dates from the dataset

# Generate more detailed scores for each day
# We will take into account:
# - Control over scheduling: who sets or cancels plans
# - Control over conversation topics
# - Teasing and undermining
# - Emotional engagement vs. detachment
# - Dominance in intellectual debates

detailed_power_scores_full = {
    '2015-04-20': 0.5,   # Elizaveta teasingly sets the frame
    '2015-04-24': -1.8,  # Sergey tries to set plans, but Elizaveta controls scheduling
    '2015-04-25': 1.5,   # Sergey successfully arranges a meeting, regains some control
    '2015-04-26': -2.0,  # Sergey delays, Elizaveta takes control of the flow
    '2015-04-28': 0.3,   # Balanced teasing and intellectual discussions
    '2015-04-29': -1.2,  # Scheduling power play (Elizaveta forces early timing)
    '2015-04-30': 0.7,   # Sergey contributes insights, but Elizaveta keeps some control
    '2015-05-01': -1.5,  # Elizaveta dominates ideological discussion
    '2015-05-02': 0.6,   # Intellectual exchange rebalances slightly
    '2015-05-03': -1.7,  # Sergey shows vulnerability, Elizaveta reframes it
    '2015-05-04': 0.4,   # Sergey maintains engagement, small gain
    '2015-05-05': -1.1,  # Elizaveta controls discussions
    '2015-05-06': -0.5,  # She subtly reinforces her dominance
    '2015-05-07': 0.3,   # Sergey adapts better
    '2015-05-08': -0.8,  # Elizaveta dictates meeting structure
    '2015-05-09': 0.5,   # Sergey successfully engages
    '2015-05-10': -1.3,  # Elizaveta reframes the emotional aspect again
    '2015-05-11': 0.8,   # Sergey has an upper hand in tech talk
    '2015-05-12': -0.9,  # Elizaveta keeps framing the narrative
    '2015-05-13': 0.4,   # Balanced day
    '2015-05-14': -1.1,  # Sergey follows, Elizaveta leads
    '2015-05-15': 0.3,   # A bit of intellectual rebalancing
    '2015-05-16': -1.0,  # Elizaveta dictates again
    '2015-05-17': 0.5,   # Sergey asserts himself slightly
    '2015-05-18': -0.7,  # Elizaveta keeps emotional framing
    '2015-05-19': 0.6,   # Sergey gets a stronger voice
    '2015-05-20': -1.2,  # Elizaveta controls again
    '2015-05-21': 0.8,   # Sergey takes initiative
    '2015-05-22': -0.9,  # Elizaveta redirects
    '2015-05-23': 0.4,   # Equal footing
    '2015-05-24': -1.3,  # Power imbalance in her favor again
    '2015-05-25': 0.2,   # Slight shift back
    '2015-05-26': -1.0,  # Sergey concedes
    '2015-05-27': 0.6,   # He pushes back
    '2015-05-28': -0.8,  # She manages to set the pace again
    '2015-05-29': 0.3,   # Sergey regains some ground
    '2015-05-30': -1.5,  # Major dominance shift to Elizaveta
    '2015-05-31': 0.9,   # Sergey asserts himself more
    '2015-06-01': -1.0,  # Again, subtle undermining by Elizaveta
    '2015-06-02': 0.5,   # A more neutral day
    '2015-06-03': -1.2,  # She redirects discussions
    '2015-06-04': 0.8,   # Sergey pushes a bit back
    '2015-06-05': -0.9,  # She still has the last word
    '2015-06-06': 0.4,   # Ends on a slight balance
}

# Convert to a DataFrame for cumulative analysis
df_full_power_dynamics = pd.DataFrame.from_dict(detailed_power_scores_full, orient='index', columns=['Daily Power Shift'])
df_full_power_dynamics['Cumulative Power Imbalance'] = df_full_power_dynamics['Daily Power Shift'].cumsum()

# Plot the cumulative power imbalance over time
plt.figure(figsize=(12, 6))
plt.plot(df_full_power_dynamics.index, df_full_power_dynamics['Cumulative Power Imbalance'], marker='o', linestyle='-', label="Cumulative Power Imbalance (Sergei - Elizaveta)")

# Formatting the plot
plt.axhline(y=0, color='gray', linestyle='--', linewidth=0.8)  # Neutral line
plt.xticks(rotation=45)
plt.xlabel("Date")
plt.ylabel("Cumulative Power Imbalance")
plt.title("Cumulative Power Imbalance Over Time (Sergei - Elizaveta)")
plt.legend()
plt.grid(True)

# Show the plot
plt.show()

# Display the table with detailed power shifts and cumulative sum
tools.display_dataframe_to_user(name="Full Cumulative Power Imbalance", dataframe=df_full_power_dynamics)

