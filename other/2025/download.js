const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

while(true) {

const runWork = async () => {
// Step 1: Find the <a> inside a button with text "View Document"

let viewDocumentLinksList = Array.from(document.querySelectorAll('#DocumentsPrintSection a')).filter(
  a => a.textContent.trim() === "View Document"
);

const getId = (s) => s.id;
const removeDuplicates = (array, getProperty) => {
  return Array.from(
    new Map(array.map(item => [getProperty(item), item])).values()
  );
};


//viewDocumentLinksList.sort((x, y) => x.id.localeCompare(y.id));
// viewDocumentLinksList = removeDuplicates(viewDocumentLinksList, getId);
// viewDocumentLinksList = viewDocumentLinksList.slice(0, 1);

console.log(viewDocumentLinksList);

for (const [i, viewDocumentLink] of viewDocumentLinksList.entries()) {

	const currentId = String(i) + '-sb-download';
	
	console.log(currentId);

	if(localStorage.getItem(currentId)) {
	    console.log('already downloaded', currentId);
		continue;
	}

	if (viewDocumentLink.href) {
	  // Step 2: Open the link in a new tab
	  const newTab = window.open(viewDocumentLink.href, '_blank');
	  console.log("Opened new tab with the document link.");

	  // Step 3: Wait for the new page to load and click the download button
	  const promise = new Promise((resolve, reject) => {
		  const checkDownloadButton = setInterval(() => {
			if (newTab && newTab.document && newTab.document.readyState === "complete") {
			  try {
				const downloadButton = Array.from(newTab.document.querySelectorAll('a')).find(
				  el => el.textContent.trim().toLowerCase().includes("download")
				);

				if (downloadButton) {
				  downloadButton.download = viewDocumentLink.id + ".pdf";
				  downloadButton.click();
				  newTab.close();
				  console.log("Clicked the download button.");
				  clearInterval(checkDownloadButton);
				  resolve(downloadButton.href);
				  localStorage.setItem(currentId, 'true');
				} 
				
				
			  } catch (error) {
				console.error("Error accessing the new tab:", error);
				reject(error);
			  }
			}
		  }, 1000); // Check every 1 second until the page loads
		});
		await promise;
	} else {
	  console.warn("'View Document' link not found.");
	}
}
}

await runWork();
await sleep(1000);
}
