import requests
import urllib.parse

# Spotify App Credentials
CLIENT_ID = "572bb3a9a1954592ac3c5be7854a99ef"
CLIENT_SECRET = "8376dae9ef5d4569a66e9202ea15f200"
REDIRECT_URI = "http://localhost/spotify_callback"

# Spotify OAuth URLs
AUTH_URL = "https://accounts.spotify.com/authorize"
TOKEN_URL = "https://accounts.spotify.com/api/token"

# Required Permissions (Scopes)
SCOPES = "playlist-modify-private playlist-modify-public user-read-private user-read-email"

# Step 1: Generate Authorization URL
params = {
    "client_id": CLIENT_ID,
    "response_type": "code",
    "redirect_uri": REDIRECT_URI,
    "scope": SCOPES,
}
auth_url = f"{AUTH_URL}?{urllib.parse.urlencode(params)}"

print("\n🔗 Open this link in your browser to authorize:")
print(auth_url)

# Step 2: Paste Authorization Code
auth_code = input("\n🔑 Paste the 'code' from the redirected URL: ")

# Step 3: Exchange Code for Access Token
response = requests.post(
    TOKEN_URL,
    data={
        "grant_type": "authorization_code",
        "code": auth_code,
        "redirect_uri": REDIRECT_URI,
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
    },
    headers={"Content-Type": "application/x-www-form-urlencoded"},
)

# Step 4: Parse Response
if response.status_code == 200:
    token_data = response.json()
    print("\n✅ Access Token Received!")
    print(f"Access Token: {token_data['access_token']}")
    print(f"Refresh Token: {token_data['refresh_token']}")
    print(f"Expires in: {token_data['expires_in']} seconds")
else:
    print("\n❌ Failed to get token:", response.json())

