import requests
import os
import json
import pandas as pd
from tqdm import tqdm

import time

# Your access token
access_token = 'BQCZ6N0wIZkj965O7EJX9Gl2-PA5woQxXyfwl6GmmR3e_qIcGd0rjyitNhlKmTiLixEujq2xVVgx7ikxjs3T9Lt8yweIQl9V3NydDqZTrGfmd2hhTjxNuaADDSWnekHotcunF1REDXksQ1Ds5ceBy53GrUwLmalaopM1TC1CYoZpsDKmgbPQx44BhRl4bDHPUncrSQ4nFfivMXpZrPUPqsZTrmpgA6_dN-0tlCkKaEUE634KIM-w6bCwWCldQMNNlgATg2jCxU3QF-D5y0NqKpclAC2eySRTfkyxSlXQsdlJdUMu'
#access_token = 'AQCTTxuxHObGEWqtZ5RaCbkD4qLRyvU0IZi_huz9a53DeNaI-6NxzVKSn8kqw_Ox41V9ONt--krzZIG0YowsQbUzRgrjGkQfTjVA8qE3yJE9RVE_Pf55ahiMKtku3BACtcuvxtsALXuanhxeOFIRWeqyTWudXjmluoRsGvcfwdFN_sHQeCwfE2TBZ1eBdkuHXJfIaSqN2hxyJwbGi3p_1m7gnDq75VoDNiq0cXFUIxUdbpPKpu2JRO1LUWYckVN1Jw2nDlGjyBEijlbCqax-hxEJQZz7_rWZ_n8' #AQA3TYQes-cEyXyX8LQRAVI8_CdjN35TqQ_Pt9sfYGeVK3sSDJo_6OfBRd11UjAZIScHVAxgx0qzqLPQqRgyc8HHpJMbYQVpYSBnmZ2Gl6nezzlBUkFd_rGsmJualxCoMfnJoYXOYV3L9X1Gow05RbOL89XD9yjYgSPq1cH_ZqHkdrlqsd2FVtxhop_zxYf82OJ6np5loREfw5RwMLiBtKBIZ30Uw7z-YuSBS-uxEktLGF48KfY' #BQANRfujlzjqD3cxNNu9hgCWeiLwzlq7lDrQHNNy5SCZedojxo8DG5IYTIq2h0ft4sVsD3sGgWJ4NuOuEyV_gF53NccbS4eOmb5Tf1Wyjamcwc_OOD7H0l8_o4YVKxcnPJ_hfRcnLD8'

# Headers for API requests
headers = {
    'Authorization': f'Bearer {access_token}',
    'Content-Type': 'application/json'
}

# 1. Retrieve User ID
user_profile_url = 'https://api.spotify.com/v1/me'
response = requests.get(user_profile_url, headers=headers)
response.raise_for_status()
user_id = response.json()['id']
print(user_id)

# 2. Create Playlist
#create_playlist_url = f'https://api.spotify.com/v1/users/{user_id}/playlists'
#playlist_data = {
#    'name': 'Before 2020',
#    'public': True,
#    'description': 'A collection of songs played before 2020.'
#}
#response = requests.post(create_playlist_url, headers=headers, json=playlist_data)
#response.raise_for_status()
#playlist_id = response.json()['id']
#print(playlist_id)
playlist_id = '6lDpgWBJHBGvPCEZq5gORK'

json_file_path = 'Desktop/spotify_json/'

# List files inside the extracted directory
extracted_json_files = os.listdir(json_file_path)
extracted_json_files

# Load and inspect the structure of both JSON files
json_files_data = {}

for file in extracted_json_files:
    file_path = os.path.join(json_file_path, file)
    with open(file_path, 'r', encoding='utf-8') as f:
        json_files_data[file] = json.load(f)

# Display the top-level keys of both files
{file: list(data[:2]) if isinstance(data, list) else data.keys() for file, data in json_files_data.items()}

# Extract unique songs from both files
unique_songs = {}
songs = []

for file, data in json_files_data.items():
    for entry in data:
        track_name = entry.get("master_metadata_track_name")
        artist_name = entry.get("master_metadata_album_artist_name")
        spotify_uri = entry.get("spotify_track_uri")

        if track_name and artist_name and spotify_uri:
            songs.append(spotify_uri)

print(len(songs))
songs = pd.DataFrame({'uri': songs})

# Count occurrences
frequency = songs['uri'].value_counts()

# Convert to DataFrame
df_frequency = frequency.reset_index()
df_frequency.columns = ['uri', 'freq']

# Display results
print(df_frequency)

df_freq = df_frequency.drop_duplicates(subset=['uri'])


# 3. Add Songs to Playlist in Batches
track_uris = list(df_freq.uri)
    # List of your 7,001 track URIs
#]
print(track_uris)

batch_size = 100
for i in tqdm(range(0, len(track_uris), batch_size)):
    batch = track_uris[i:i + batch_size]
    add_tracks_url = f'https://api.spotify.com/v1/playlists/{playlist_id}/tracks'
    response = requests.post(add_tracks_url, headers=headers, json={'uris': batch})
    response.raise_for_status()
    time.sleep(1)  # To respect rate limits

