# Now, let's analyze the power dynamics in the conversation day by day.
import pandas as pd
# We will assign a score to Sergei and Elizaveta for each day based on:
# - Who controls the scheduling
# - Who dictates the tone of the conversation
# - Who teases and who adapts
# - Who shows vulnerability and who stays emotionally composed
# - Who dominates intellectually

# We'll process the extracted text to track the dynamics over time.

from collections import defaultdict

# Placeholder for daily scores
daily_power_scores = defaultdict(lambda: {'Sergei': 0, 'Elizaveta': 0})

# Manually analyzing key moments and assigning values based on observations.

# Example scoring:
# +1 if the person initiates and successfully schedules a meeting.
# -1 if they chase the other person or adapt to their schedule.
# +1 for controlling the tone of the conversation.
# -1 for adapting to teasing or being subtly undermined.
# +1 for intellectual dominance (setting discussion topics).
# -1 for being tested or subtly dismissed.

# We will now go day by day, analyzing the balance of power.

# Data Entry - Adjust based on patterns observed in the conversation.

# April 20, 2015
daily_power_scores['2015-04-20']['Sergei'] += 0  # Playful start, neutral
daily_power_scores['2015-04-20']['Elizaveta'] += 1  # Playful teasing, sets the frame

# April 24, 2015
daily_power_scores['2015-04-24']['Sergei'] -= 1  # Chasing plans, adapting
daily_power_scores['2015-04-24']['Elizaveta'] += 1  # Controls the flow, decides scheduling

# April 25, 2015
daily_power_scores['2015-04-25']['Sergei'] += 1  # Pushes for meeting successfully
daily_power_scores['2015-04-25']['Elizaveta'] -= 1  # Concedes to his schedule

# April 26, 2015
daily_power_scores['2015-04-26']['Sergei'] -= 1  # He delays, making her wait
daily_power_scores['2015-04-26']['Elizaveta'] += 1  # Gains control by commenting on his delay

# April 28, 2015
daily_power_scores['2015-04-28']['Sergei'] += 1  # Conversational dominance, shares stories
daily_power_scores['2015-04-28']['Elizaveta'] += 1  # Engages but keeps teasing

# April 29, 2015
daily_power_scores['2015-04-29']['Sergei'] -= 1  # Loses scheduling, forced into 8 AM joke
daily_power_scores['2015-04-29']['Elizaveta'] += 1  # Controls the plan

# April 30, 2015
daily_power_scores['2015-04-30']['Sergei'] += 1  # Recognizes her wisdom, positive reinforcement
daily_power_scores['2015-04-30']['Elizaveta'] += 1  # Sets intellectual topics, keeps control

# May 1, 2015
daily_power_scores['2015-05-01']['Sergei'] -= 1  # Takes advice, follows Elizaveta's lead
daily_power_scores['2015-05-01']['Elizaveta'] += 1  # Frames discussions, sets ideological tone

# May 2, 2015
daily_power_scores['2015-05-02']['Sergei'] += 1  # Intellectual exchange, rebalances
daily_power_scores['2015-05-02']['Elizaveta'] += 1  # Teases, controls discussion

# May 3, 2015
daily_power_scores['2015-05-03']['Sergei'] -= 1  # Shows vulnerability, she reframes
daily_power_scores['2015-05-03']['Elizaveta'] += 1  # Reframes emotional narrative

# May 4, 2015
daily_power_scores['2015-05-04']['Sergei'] += 1  # Keeps engagement active
daily_power_scores['2015-05-04']['Elizaveta'] += 1  # Teasing control

# May 5, 2015
daily_power_scores['2015-05-05']['Sergei'] -= 1  # Adapts to Elizaveta's discussions
daily_power_scores['2015-05-05']['Elizaveta'] += 1  # Dominates ideological discussion

# Convert to a DataFrame for visualization
df_power_dynamics = pd.DataFrame.from_dict(daily_power_scores, orient='index')

# Calculate the power imbalance (Sergei's power - Elizaveta's power)
df_power_dynamics['Power Imbalance'] = df_power_dynamics['Sergei'] - df_power_dynamics['Elizaveta']

# Display the results
print(df_power_dynamics)
