#!/bin/bash

DIR=repositories
(for a in $DIR/*; do git -C $a shortlog -sne --all; done)|grep --color=NEVER -aoE "<.*"|sort|uniq|tee 03contributors.txt
