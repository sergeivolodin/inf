#!/bin/bash

DIR=repositories
mkdir -p $DIR

for url_part in $(sed 1,1d 00repositories.csv | sed -e 's/,/\//g')
do
	cd $DIR
	git clone git@github.com:${url_part}.git
	cd ..
done
