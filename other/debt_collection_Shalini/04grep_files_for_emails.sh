#!/bin/bash

# https://stackoverflow.com/questions/2898463/using-grep-to-find-all-emails

DIR=repositories
grep -aEiorh '([[:alnum:]_.-]+@[[:alnum:]_.-]+?\.[[:alpha:].]{2,6})' "$@" $DIR | sort | uniq | tee 04emails.txt
